#!/usr/bin/env python
# -*- coding: utf-8 -*-
from platform import python_version
class LZW:
    tabelaCodificacao = [];
    tabelaDecodificacao = [];
    def procurarNaTabela(self,palavra,tabela):
        for i in range(len(tabela)):
            if tabela[i] == palavra:
                return i+256
        return -1;

    def codificar(self,entrada):
        retorno = ""
        terminou = False
        posicaoNaEntrada = 1#como a entrada é uma string, o char no qual se irá navegar
        charDaTabelaConhecido = ord(entrada[0])#qual é o char que está analisando 

        if(len(entrada) == 0):
            retorno = ""
            return retorno
            #print("s1");
        
        if(len(entrada) == 1):
            retorno += str(charDaTabelaConhecido) + " "
            #print("S2")

        while(terminou == False):
            if(charDaTabelaConhecido <= 255):
                if(posicaoNaEntrada >= len(entrada)):
                    retorno += str(charDaTabelaConhecido) + " "
                    break;
                temporario = self.procurarNaTabela(str(chr(charDaTabelaConhecido)) + entrada[posicaoNaEntrada],self.tabelaCodificacao)
               # print(str(temporario) + " s1")
            else:
                if(posicaoNaEntrada >= len(entrada)):
                    retorno += str(charDaTabelaConhecido) + " "
                    break;
                temporario = self.procurarNaTabela(self.tabelaCodificacao[charDaTabelaConhecido-256] + entrada[posicaoNaEntrada],self.tabelaCodificacao)
                #print(str(temporario) + " s2")
            

            if  temporario != -1: #existe na tabela a sequência
                charDaTabelaConhecido = temporario;
                #print(charDaTabelaConhecido)
                posicaoNaEntrada += 1
                if(posicaoNaEntrada >= len(entrada)): #Já foram todos os caracteres
                    retorno += str(charDaTabelaConhecido) + " "
                    terminou = True
                    
            else: # a junção não existe na tabela
                #Coloca a junção na tabela
                if(charDaTabelaConhecido <= 255): # 1 caractere
                    self.tabelaCodificacao.append(str(chr(charDaTabelaConhecido)) + entrada[posicaoNaEntrada]);
                else: # caractere composto
                    self.tabelaCodificacao.append(self.tabelaCodificacao[charDaTabelaConhecido-256] + entrada[posicaoNaEntrada]);

                #armazena o pedaço anteriormente conhecido
                retorno += str(charDaTabelaConhecido) + " "
                #print(str(charDaTabelaConhecido) + " s3")

                #passa para o próximo
                if(posicaoNaEntrada < len(entrada)):
                    charDaTabelaConhecido = ord(entrada[posicaoNaEntrada])
                    posicaoNaEntrada += 1
                else:
                    terminou = True #Todos os caracteres foram convertidos
        retorno = retorno[:-1] #Tira último espaço
        return retorno



    def decodificar(self,entrada):
        partesEntrada = entrada.split(" ")
        retorno = ""
        terminou = False
        posicaoNaEntrada = 0#como a entrada é uma string, o char no qual se irá navegar
        charDaTabela = int(partesEntrada[0])#qual é o char que está analisando 
        while(terminou == False):
            if(posicaoNaEntrada < len(partesEntrada)-1): #tem próximo
                if(int(partesEntrada[posicaoNaEntrada]) < 255): #ASCII padrão
                    retorno += str(chr(int(partesEntrada[posicaoNaEntrada])))
                    if(int(partesEntrada[posicaoNaEntrada + 1]) < 255): # A próxima parte também está na ASCII
                        self.tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                    else: # a próxima parte está na tabela
                        #pega só o primeiro caractere
                        self.tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])

                    
                else: #tabela
                    retorno += str(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256])
                    
                    if(int(partesEntrada[posicaoNaEntrada + 1]) < 255): # A próxima parte está na ASCII
                        self.tabelaDecodificacao.append(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                    else: # a próxima parte também está na tabela
                        self.tabelaDecodificacao.append(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])

                posicaoNaEntrada += 1
            else: #é o último
                if(int(partesEntrada[posicaoNaEntrada]) < 255): #ASCII padrão
                    retorno += str(chr(int(partesEntrada[posicaoNaEntrada])))
                else:#tabela
                    retorno += str(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256])
                terminou = True
                
    

            
        return retorno

        

        
if(python_version()[0] != "3"): #Se estiver usando uma outra versão, alguns comandos podem ser incompatíveis,
    print(python_version()[0])  #como o próprio input
    print("Execute na versão 3 do python")
    exit(2)


lzw = LZW() #Codificador e decodificador

'''
Código do programa que interage com o usuário
'''
onLoop = True
while(onLoop == True):
    escolha = input("Digite 0 para comprimir, 1 para descomprimir ou 2 para sair")
    if escolha == "0":
        nomeDoArquivoEntrada = input("Qual o nome do arquivo a ser codificado?");
        #nomeDoArquivoEntrada = "entrada.txt"
        arquivo = open(nomeDoArquivoEntrada,'r',encoding="Latin-1")
        conteudo = arquivo.read()
        arquivo.close()
        print("Codificando tradução...")
        conteudoCodificado = lzw.codificar(conteudo)
        #salvar
        print("Convertendo para binário...")
        #converter para binário
        #conteudoCodificado = conteudoCodificado.encode()
        tabela = ""
        for i in range(len(lzw.tabelaCodificacao)):
            tabela += lzw.tabelaCodificacao[i] + "\n"
        arquivo = open("tabelaCodificacao",'w')
        arquivo.write(tabela)
        arquivo.close()

        nomeDoArquivoSaida = input("Qual o nome do arquivo a ser salvo?")
        arquivo = open(nomeDoArquivoSaida,'w')
        arquivo.write(conteudoCodificado)
        arquivo.close()
        #print(conteudoCodificado)
    elif escolha == "1":
        nomeDoArquivoEntrada = input("Qual o nome do arquivo a ser decodificado?")
        arquivo = open(nomeDoArquivoEntrada,'r')
        conteudo = arquivo.read()
        arquivo.close()

        #conveter conteúdo para ascii
        #conteudo = conteudo.decode()

        nomeDoArquivoSaida = input("Qual o nome do arquivo a ser salvo?")

        conteudoDecodificado = lzw.decodificar(conteudo)
        arquivo = open(nomeDoArquivoSaida,'w',encoding="Latin-1")
        arquivo.write(conteudoDecodificado)
        arquivo.close()
        tabela = ""
        for i in range(len(lzw.tabelaDecodificacao)):
            tabela += lzw.tabelaDecodificacao[i] + "\n"
        arquivo = open("tabelaDecodificacao",'w')
        arquivo.write(tabela)
        arquivo.close()
    elif escolha == "2":
        onLoop = False
        