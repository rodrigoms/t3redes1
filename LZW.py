#!/usr/bin/env python
# -*- coding: utf-8 -*-
from platform import python_version
class LZW:

    #variáveis compartilhadas
    tamanhoMaximoTabelas = 512 #Se a tabela já tiver adicionado esse número de
                              #elementos, mais nenhum deverá ser adicionado
    
    #variáveis codificação
    tabelaCodificacao = [];



    #variáveis decodificação
    tabelaDecodificacao = [];


    '''
    Esta função percorre uma tabela em busca de uma palavra.
    Ao encontrar a palavra, retorna o indice da tabela + 256
    Se não encontrar, retorna -1
    '''
    def procurarNaTabela(self,palavra,tabela):
        for i in range(len(tabela)):
            if tabela[i] == palavra:
                return i+256
        return -1;

    '''
    Esta função realiza a codificação LZW e retorna uma string
    com o texto codificado
    '''

    def codificar(self,entrada):
        retorno = ""
        terminou = False
        posicaoNaEntrada = 1#como a entrada é uma string, o char no qual se irá navegar
        charDaTabelaConhecido = ord(entrada[0])#qual é o char que está analisando 
        flagDuplaLetraCodificada = [] #Cada letra, se vier em sequência de iguais, na primeira
                                      #ocorrência dessa sequencia, o segundo elemento dessa sequência
                                      #deve ser enviado como se estivesse sozinho, sem formar
                                      #sequencia tripla em memória e nem concatenar com o próximo
        for i in range(255):
            flagDuplaLetraCodificada.append(0) #Nenhum teve uma dessas ocorrências


        if(len(entrada) == 0):
            retorno = ""
            return retorno
            #print("s1");
        
        if(len(entrada) == 1):
            retorno += str(charDaTabelaConhecido) + " "
            #print("S2")

        while(terminou == False):
            if(charDaTabelaConhecido <= 255):
                if(posicaoNaEntrada >= len(entrada)):
                    retorno += str(charDaTabelaConhecido) + " "
                    break;
                if(str(chr(charDaTabelaConhecido)) == entrada[posicaoNaEntrada]):# Caso especial,letras seguidas
                    if(flagDuplaLetraCodificada[charDaTabelaConhecido] == 0): #Não enviou o segundo ainda; caso especial.
                        retorno += str(charDaTabelaConhecido) + " "
                        charDaTabelaConhecido = ord(entrada[posicaoNaEntrada])
                        posicaoNaEntrada += 1
                        flagDuplaLetraCodificada[charDaTabelaConhecido] = 1
                        continue;
                    else:
                        flagDuplaLetraCodificada[charDaTabelaConhecido] = 0

                temporario = self.procurarNaTabela(str(chr(charDaTabelaConhecido)) + entrada[posicaoNaEntrada],self.tabelaCodificacao)
               # print(str(temporario) + " s1")
            else:
                if(posicaoNaEntrada >= len(entrada)):
                    retorno += str(charDaTabelaConhecido) + " "
                    break;
                
                if(self.tabelaCodificacao[charDaTabelaConhecido-256] == entrada[posicaoNaEntrada]):# Caso especial,letras seguidas
                    if(flagDuplaLetraCodificada[charDaTabelaConhecido] == 0): #Não enviou o segundo ainda; caso especial.
                        retorno += self.tabelaCodificacao[charDaTabelaConhecido-256] + " "
                        charDaTabelaConhecido = ord(entrada[posicaoNaEntrada])
                        posicaoNaEntrada += 1
                        flagDuplaLetraCodificada[charDaTabelaConhecido] = 1
                        continue;
                    else:
                        flagDuplaLetraCodificada[charDaTabelaConhecido] = 0


                temporario = self.procurarNaTabela(self.tabelaCodificacao[charDaTabelaConhecido-256] + entrada[posicaoNaEntrada],self.tabelaCodificacao)
                #print(str(temporario) + " s2")
            

            if  temporario != -1: #existe na tabela a sequência
                charDaTabelaConhecido = temporario;
                #print(charDaTabelaConhecido)
                posicaoNaEntrada += 1
                if(posicaoNaEntrada >= len(entrada)): #Já foram todos os caracteres
                    retorno += str(charDaTabelaConhecido) + " "
                    terminou = True
                    
            else: # a junção não existe na tabela
                #Coloca a junção na tabela
                if(charDaTabelaConhecido <= 255): # 1 caractere
                    if(len(self.tabelaCodificacao) < self.tamanhoMaximoTabelas):
                        self.tabelaCodificacao.append(str(chr(charDaTabelaConhecido)) + entrada[posicaoNaEntrada]);
                        flagDuplaLetraCodificada.append(0)
                else: # caractere composto
                    if(len(self.tabelaCodificacao) < self.tamanhoMaximoTabelas):
                        self.tabelaCodificacao.append(self.tabelaCodificacao[charDaTabelaConhecido-256] + entrada[posicaoNaEntrada]);
                        flagDuplaLetraCodificada.append(0)
                #armazena o pedaço anteriormente conhecido
                retorno += str(charDaTabelaConhecido) + " "
                #print(str(charDaTabelaConhecido) + " s3")

                #passa para o próximo
                if(posicaoNaEntrada < len(entrada)):
                    charDaTabelaConhecido = ord(entrada[posicaoNaEntrada])
                    posicaoNaEntrada += 1
                else:
                    terminou = True #Todos os caracteres foram convertidos
        retorno = retorno[:-1] #Tira último espaço
        return retorno


    '''
    Esta função verifica se determinado texto consta em uma tabela
    A diferença dela com o procurarNaTabela é o valor de retorno,
    que nesse caso é True se existir e False se não existir
    '''
    def estaNaTabela(self,texto,tabela):
        for i in range(len(tabela)):
            #print("Comparando " + tabela[len(tabela) - i - 1] + " com " + texto);
            if(tabela[len(tabela) - i - 1] == texto):
                return True;
        #print("Não está na tabela!")
        return False
    '''
    Esta função é para debbuging e lista os valores contidos em uma tabela
    '''

    def mostrarTabela(self,tabela):
        for i in range(len(tabela)):
            print(tabela[i] + "\n")

    '''
    Esta função realiza a decodificação de um texto, retornado o texto decodificado.

    '''
    def decodificar(self,entrada):
        partesEntrada = entrada.split(" ")
        retorno = ""
        terminou = False
        posicaoNaEntrada = 0#como a entrada é uma string, o char no qual se irá navegar
        charDaTabela = int(partesEntrada[0])#qual é o char que está analisando 

        flagDuplaLetraDecodificada = []
        for i in range(255):
            flagDuplaLetraDecodificada.append(0) #Nenhum teve uma dessas ocorrências


        while(terminou == False):
            if(posicaoNaEntrada < len(partesEntrada)-1): #tem próximo
                if(int(partesEntrada[posicaoNaEntrada]) < 255): #ASCII padrão

                    retorno += str(chr(int(partesEntrada[posicaoNaEntrada])))
                    if(int(partesEntrada[posicaoNaEntrada + 1]) < 255): # A próxima parte também está na ASCII
                        if(self.estaNaTabela(str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))),self.tabelaDecodificacao) == False):
                            if(len(self.tabelaDecodificacao) < self.tamanhoMaximoTabelas):
                                self.tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                    else: # a próxima parte está na tabela
                        #pega só o primeiro caractere
                        if(self.estaNaTabela(str(chr(int(partesEntrada[posicaoNaEntrada]))) + self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256],self.tabelaDecodificacao) == False):
                            if(len(self.tabelaDecodificacao) < self.tamanhoMaximoTabelas):
                                self.tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                        
                else: #tabela
                    retorno += str(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256])
                    
                    #Como a segunda letra vem sem codificação, precisamos evitar uma dupla tabelagem
                    #por parte do receptor
                    if(int(partesEntrada[posicaoNaEntrada + 1]) < 255): # A próxima parte está na ASCII
                        if(self.estaNaTabela(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))),self.tabelaDecodificacao) == False):
                            if(len(self.tabelaDecodificacao) < self.tamanhoMaximoTabelas):
                                self.tabelaDecodificacao.append(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))

                    else: # a próxima parte também está na tabela
                        if(self.estaNaTabela(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1],self.tabelaDecodificacao) == False):
                            if(len(self.tabelaDecodificacao) < self.tamanhoMaximoTabelas):
                                self.tabelaDecodificacao.append(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                posicaoNaEntrada += 1
            else: #é o último
                if(int(partesEntrada[posicaoNaEntrada]) < 255): #ASCII padrão
                    retorno += str(chr(int(partesEntrada[posicaoNaEntrada])))
                else:#tabela
                    retorno += str(self.tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256])
                terminou = True
                
    

            
        return retorno


        
if(python_version()[0] != "3"): #Se estiver usando uma outra versão, alguns comandos podem ser incompatíveis,
    print(python_version()[0])  #como o próprio input
    print("Execute na versão 3 do python")
    exit(2)


lzw = LZW() #Codificador e decodificador

'''
Código do programa que interage com o usuário
'''
onLoop = True
while(onLoop == True):
    escolha = input("Digite 0 para comprimir, 1 para descomprimir ou 2 para sair")
    if escolha == "0":
        nomeDoArquivoEntrada = input("Qual o nome do arquivo a ser codificado?");
        #nomeDoArquivoEntrada = "entrada.txt"
        arquivo = open(nomeDoArquivoEntrada,'r',encoding="Latin-1")
        conteudo = arquivo.read()
        arquivo.close()
        print("Codificando tradução...")
        conteudoCodificado = lzw.codificar(conteudo)
        #salvar
        print("Convertendo para binário...")
        #converter para binário
        #conteudoCodificado = conteudoCodificado.encode()
        tabela = ""
        for i in range(len(lzw.tabelaCodificacao)):
            tabela += lzw.tabelaCodificacao[i] + "\n"
        arquivo = open("tabelaCodificacao",'w')
        arquivo.write(tabela)
        arquivo.close()

        nomeDoArquivoSaida = input("Qual o nome do arquivo a ser salvo?")
        arquivo = open(nomeDoArquivoSaida,'w')
        arquivo.write(conteudoCodificado)
        arquivo.close()
        #print(conteudoCodificado)
    elif escolha == "1":
        nomeDoArquivoEntrada = input("Qual o nome do arquivo a ser decodificado?")
        arquivo = open(nomeDoArquivoEntrada,'r')
        conteudo = arquivo.read()
        arquivo.close()

        #conveter conteúdo para ascii
        #conteudo = conteudo.decode()

        nomeDoArquivoSaida = input("Qual o nome do arquivo a ser salvo?")

        conteudoDecodificado = lzw.decodificar(conteudo)
        arquivo = open(nomeDoArquivoSaida,'w',encoding="Latin-1")
        arquivo.write(conteudoDecodificado)
        arquivo.close()
        tabela = ""
        for i in range(len(lzw.tabelaDecodificacao)):
            tabela += lzw.tabelaDecodificacao[i] + "\n"
        arquivo = open("tabelaDecodificacao",'w')
        arquivo.write(tabela)
        arquivo.close()
    elif escolha == "2":
        onLoop = False
        
