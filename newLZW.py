#!/usr/bin/env python
# -*- coding: utf-8 -*-
from platform import python_version
class LZW:

    tamanhoMaximoTabela = 100

   




    def procurarNaTabela(self,palavra,tabela):
        for i in range(len(tabela)):
            if tabela[i] == palavra:
                return i+256
        return -1;

    def codificar(self,entrada):
        try:
            tabelaCodificacao = [];
            flagTriploIgual = []
            retorno = ""
            terminou = False
            posicaoNaEntrada = 1#como a entrada é uma string, o char no qual se irá navegar
            charDaTabelaConhecido = ord(entrada[0])#qual é o char que está analisando 
            letraAnterior = -1
            for i in range(255):
                flagTriploIgual.append(0)
    
            if(len(entrada) == 0):
                retorno = ""
                return retorno
                #print("s1");
            
            if(len(entrada) == 1):
                retorno += str(charDaTabelaConhecido) + " "
                #print("S2")
    
            while(terminou == False):
                if(charDaTabelaConhecido <= 255):
                    if(posicaoNaEntrada >= len(entrada)):
                        retorno += str(charDaTabelaConhecido) + " "
                        break;
                    temporario = self.procurarNaTabela(str(chr(charDaTabelaConhecido)) + entrada[posicaoNaEntrada],tabelaCodificacao)
                    if(letraAnterior != -1):
                        if((str(chr(charDaTabelaConhecido)) == entrada[posicaoNaEntrada])):
                            if(letraAnterior == ord(entrada[posicaoNaEntrada])):
                                if(flagTriploIgual[letraAnterior] == 0):
                                    retorno += str(charDaTabelaConhecido) + " "
                                    flagTriploIgual[int(letraAnterior)] = 1
                                    posicaoNaEntrada+=1
                                    continue;
                   # print(str(temporario) + " s1")
                else:
                    if(posicaoNaEntrada >= len(entrada)):
                        retorno += str(charDaTabelaConhecido) + " "
                        break;
    
                    temporario = self.procurarNaTabela(tabelaCodificacao[charDaTabelaConhecido-256] + entrada[posicaoNaEntrada],tabelaCodificacao)
                    if(tabelaCodificacao[charDaTabelaConhecido-256] == entrada[posicaoNaEntrada]):
                            if(letraAnterior == charDaTabelaConhecido):
                                if(flagTriploIgual[letraAnterior] == 0):
                                    retorno += tabelaCodificacao[charDaTabelaConhecido-256] + " "
                                    flagTriploIgual[letraAnterior] = 1
                                    posicaoNaEntrada+=1
                                    continue;
                    #print(str(temporario) + " s2")
                
    
                if  temporario != -1: #existe na tabela a sequência
                    charDaTabelaConhecido = temporario;
                    #print(charDaTabelaConhecido)
                    posicaoNaEntrada += 1
                    if(posicaoNaEntrada >= len(entrada)): #Já foram todos os caracteres
                        retorno += str(charDaTabelaConhecido) + " "
                        letraAnterior = charDaTabelaConhecido
                        terminou = True
                        
                else: # a junção não existe na tabela
                    #Coloca a junção na tabela
                    if(charDaTabelaConhecido <= 255): # 1 caractere
                        if(len(tabelaCodificacao) < self.tamanhoMaximoTabela):
                            tabelaCodificacao.append(str(chr(charDaTabelaConhecido)) + entrada[posicaoNaEntrada]);
                    else: # caractere composto
                        if(len(tabelaCodificacao) < self.tamanhoMaximoTabela):
                            tabelaCodificacao.append(tabelaCodificacao[charDaTabelaConhecido-256] + entrada[posicaoNaEntrada]);
    
                    #armazena o pedaço anteriormente conhecido
                    retorno += str(charDaTabelaConhecido) + " "
                    letraAnterior = charDaTabelaConhecido
                    #print(str(charDaTabelaConhecido) + " s3")
    
                    #passa para o próximo
                    if(posicaoNaEntrada < len(entrada)):
                        charDaTabelaConhecido = ord(entrada[posicaoNaEntrada])
                        posicaoNaEntrada += 1
                    else:
                        terminou = True #Todos os caracteres foram convertidos
            retorno = retorno[:-1] #Tira último espaço
            return retorno
        except:
            print("Ocorreu um erro na codificação! Provavelmente seu arquivo contém caracteres inválidos!\n")


    def mostrarTabela(self,tabela):
        for i in range(len(tabela)):
            print(tabela[i] + "\n")

    def decodificar(self,entrada):
        try:
            flagIgualDecodificacao = []
            tabelaDecodificacao = []
            partesEntrada = entrada.split(" ")
            retorno = ""
            terminou = False
            posicaoNaEntrada = 0#como a entrada é uma string, o char no qual se irá navegar
            charDaTabela = int(partesEntrada[0])#qual é o char que está analisando 
    
            for i in range(255):
                flagIgualDecodificacao.append(0)
    
            while(terminou == False):
                if(posicaoNaEntrada < len(partesEntrada)-1): #tem próximo
                    if(int(partesEntrada[posicaoNaEntrada]) < 255): #ASCII padrão
    
    
                        if(int(partesEntrada[posicaoNaEntrada + 1]) < 255): # A próxima parte também está na ASCII
                            if(len(tabelaDecodificacao) > 0):
                                if(tabelaDecodificacao[len(tabelaDecodificacao)-1] != str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1])))):
                                    if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                        tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                                else:
                                    if(posicaoNaEntrada < len(partesEntrada) - 2):
                                        if(partesEntrada[posicaoNaEntrada + 2] == partesEntrada[posicaoNaEntrada]):
                                            if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                                tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                            else:
                                if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                    tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                                #print("Eu 1: " + str(chr(int(partesEntrada[posicaoNaEntrada]))) + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                        else: # a próxima parte está na tabela
                            #pega só o primeiro caractere
                            if(tabelaDecodificacao[len(tabelaDecodificacao)-1] != str(chr(int(partesEntrada[posicaoNaEntrada]))) + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1]):
                                if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                    tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                                #print("Eu 2: " + str(chr(int(partesEntrada[posicaoNaEntrada]))) + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                            else:
                                if(posicaoNaEntrada < len(partesEntrada) - 2):
                                    if((partesEntrada[posicaoNaEntrada + 2] == partesEntrada[posicaoNaEntrada]) or (int(partesEntrada[posicaoNaEntrada + 2]) >= len(tabelaDecodificacao))):
                                        if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                            tabelaDecodificacao.append(str(chr(int(partesEntrada[posicaoNaEntrada]))) + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                                        #print("Eu 3: " + str(chr(int(partesEntrada[posicaoNaEntrada]))) + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
    
                        retorno += str(chr(int(partesEntrada[posicaoNaEntrada])))
    
                        
                    else: #tabela
                        retorno += str(tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256])
    
                        if(int(partesEntrada[posicaoNaEntrada + 1]) < 255): # A próxima parte está na ASCII
                            if(tabelaDecodificacao[len(tabelaDecodificacao)-1] != tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + str(chr(int(partesEntrada[posicaoNaEntrada + 1])))):
                                if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                    tabelaDecodificacao.append(tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                                #print("Eu 4: " + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + str(chr(int(partesEntrada[posicaoNaEntrada + 1]))))
                        else: # a próxima parte também está na tabela
                            #print(int(partesEntrada[posicaoNaEntrada + 1]))
                            #print("Tabela:\n")
                            #self.mostrarTabela(tabelaDecodificacao)
                            if((int(partesEntrada[posicaoNaEntrada + 1])-256) >= len(tabelaDecodificacao) and (not((posicaoNaEntrada == len(partesEntrada)-2)))):
                                if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                    tabelaDecodificacao.append(tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256][0:1])
                                #print("Eu 5: " + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256][0:1] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256][0:1])
                            else:
                                if(tabelaDecodificacao[len(tabelaDecodificacao)-1] != tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1]):
                                    if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                        tabelaDecodificacao.append(tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                                    #print("Eu 6: " + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                                else:
                                    if(posicaoNaEntrada < len(partesEntrada) - 2): #Tem ao menos 1 após o próximo
                                        if(partesEntrada[posicaoNaEntrada+1] == partesEntrada[posicaoNaEntrada+2]):#Se for diferente não faz nada
                                            if(len(tabelaDecodificacao) < self.tamanhoMaximoTabela):
                                                tabelaDecodificacao.append(tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
    
                                            #print("Eu 7: " + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1] + tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada + 1]) - 256][0:1])
                    posicaoNaEntrada += 1
                else: #é o último
                    if(int(partesEntrada[posicaoNaEntrada]) < 255): #ASCII padrão
                        retorno += str(chr(int(partesEntrada[posicaoNaEntrada])))
    
                    else:#tabela
                        retorno += str(tabelaDecodificacao[int(partesEntrada[posicaoNaEntrada]) - 256])
    
                    terminou = True
                    
        
    
                
            return retorno
        except:
            print("Ocorreu um erro na decodificacao\n")
            print("Provavelmente o arquivo de entrada foi feito com outro codificador, ou\n")
            print("o arquivo está corrompido ou foi modificado, ou\n")
            print("faltou tratar algum caso no código!\n")
        

        
if(python_version()[0] != "3"): #Se estiver usando uma outra versão, alguns comandos podem ser incompatíveis,
    print(python_version()[0])  #como o próprio input
    print("Execute na versão 3 do python")
    exit(2)


lzw = LZW() #Codificador e decodificador

'''
Código do programa que interage com o usuário
'''
onLoop = True
while(onLoop == True):
    escolha = input("Digite 0 para comprimir, 1 para descomprimir ou 2 para sair")
    if escolha == "0":
        nomeDoArquivoEntrada = input("Qual o nome do arquivo a ser codificado?");
        #nomeDoArquivoEntrada = "entrada.txt"
        arquivo = open(nomeDoArquivoEntrada,'r',encoding="Latin-1")
        conteudo = arquivo.read()
        arquivo.close()
        print("Codificando tradução...")
        conteudoCodificado = lzw.codificar(conteudo)
        #salvar
        print("Convertendo para binário...")
        #converter para binário
        #conteudoCodificado = conteudoCodificado.encode()
        '''
        tabela = ""
        for i in range(len(lzw.tabelaCodificacao)):
            tabela += lzw.tabelaCodificacao[i] + "\n"
        arquivo = open("tabelaCodificacao",'w')
        arquivo.write(tabela)
        arquivo.close()
        '''
        nomeDoArquivoSaida = input("Qual o nome do arquivo a ser salvo?")
        arquivo = open(nomeDoArquivoSaida,'w')
        arquivo.write(conteudoCodificado)
        arquivo.close()
        #print(conteudoCodificado)
    elif escolha == "1":
        nomeDoArquivoEntrada = input("Qual o nome do arquivo a ser decodificado?")
        arquivo = open(nomeDoArquivoEntrada,'r')
        conteudo = arquivo.read()
        arquivo.close()

        #conveter conteúdo para ascii
        #conteudo = conteudo.decode()

        nomeDoArquivoSaida = input("Qual o nome do arquivo a ser salvo?")

        conteudoDecodificado = lzw.decodificar(conteudo)
        arquivo = open(nomeDoArquivoSaida,'w',encoding="Latin-1")
        arquivo.write(conteudoDecodificado)
        arquivo.close()
        '''
        tabela = ""
        for i in range(len(lzw.tabelaDecodificacao)):
            tabela += lzw.tabelaDecodificacao[i] + "\n"
        arquivo = open("tabelaDecodificacao",'w')
        arquivo.write(tabela)
        arquivo.close()
        '''
    elif escolha == "2":
        onLoop = False
        